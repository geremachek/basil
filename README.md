<h1 align="center">basil</h1>

<p align="center">An RPN calculator with a simple terminal interface</p>

<p align="center"><img src="tiny.png"></p>

**Commands:**

```
Pop off the stack: "pop", "p", "."
Swap: "swap", "s", ","
Push memory value to stack: "recall", "r", "]"
Set memory value: "store", "S", "["
Clear the stack: "clear", "c", ";" 
Switch between degrees and radians: "angle", "a", "<"
+
-
*
/
%
^
square
cube
sqrt
log
ln
logx
root
flip
sin
cos
tan
asin
acos
atan
```

**Key bindings:**

```
ESC: Exit
```
